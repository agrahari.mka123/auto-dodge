#!/bin/bash

cp $1 temp

sed -i 's/"obstacleX"://g' temp 
sed -i 's/"obstacleY"://g' temp 
sed -i 's/"obstacleSize"://g' temp 
sed -i 's/"obstacleType"://g' temp 
sed -i 's/"distanceToObstacle"://g' temp 
sed -i 's/"trexY"://g' temp 
sed -i 's/"currentSpeed"://g' temp 
sed -i 's/"action"://g' temp 
sed -i 's/"trigger"://g' temp

sed -i 's/"CACTUS_SMALL"/0/g' temp
sed -i 's/"CACTUS_LARGE"/1/g' temp
sed -i 's/"PTERODACTYL"/2/g' temp

sed -i 's/"CRASHED"/0/g' temp
sed -i 's/"DUCKING"/1/g' temp
sed -i 's/"JUMPING"/2/g' temp
sed -i 's/"RUNNING"/3/g' temp
sed -i 's/"WAITING"/4/g' temp


sed -i 's/"Run"/0/g' temp
sed -i 's/"Jump"/1/g' temp
sed -i 's/"Duck"/2/g' temp
sed -i 's/"DuckBegin"/3/g' temp
sed -i 's/"DuckRelease"/4/g' temp
sed -i 's/"duckRelease"/4/g' temp


sed -i 's/}{/}\n{/g' temp
sed -i 's/},{/}\n{/g' temp

sed -i 's/{//g' temp

sed -i 's/}//g' temp

sed -i 's/\[//g' temp

sed -i 's/]//g' temp

sed -i '/^null/d' temp

cp temp $2