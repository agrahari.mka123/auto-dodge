from sklearn import preprocessing
import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt

def sigmoid(vector):
    return  np.divide(1,(1+(math.e ** -vector)))

def cost_function(h,y):
    m = h.shape[0]
    return (1/2*m)*(np.sum(np.multiply((h-y),(h-y))))

def gradient_descent(X,Y, class_count,alpha, iterations):
    n = X.shape[1]
    J = []

    theta = np.zeros((n,class_count))
    for i in range(iterations):
        h = sigmoid(np.matmul(X,theta))
        J.append(cost_function(h,Y))
        theta = theta - ((alpha/n)* np.matmul(X.T,(h-Y)))
    return J,theta

    

import os
data = pd.read_csv('data.csv')
m,n = data.shape
print(str(m) +" records")
#Adding the bias parameter
X = np.concatenate((np.ones((m,1)),data.values[:,:-1]),axis=1)
X = np.concatenate((X[:,0:4],X[:,5:]),axis=1) # Eliminating ObstacleType from input as it seemed to have a deterring effect on the model
class_count = 1 
#Normalizing the input parameters
X[:,1] = X[:,1]/100
X[:,2] = X[:,2]/10
X[:,2] = X[:,4]/5

print("Input data shape "+ str(X.shape))
y = data.values[:,-1].astype(int)
y = y.reshape(len(y),1)
Y = np.zeros((m,class_count)).astype(int)

iter = 10000
J,theta = gradient_descent(X,y,class_count,0.00001,iter)

plt.plot(np.arange(0,iter),J[0:iter])

plt.show()
for theta_i in theta.T:
    print("[",end=' ')
    print(",".join([str(i) for i in theta_i]),end =' ')
    print("],")



