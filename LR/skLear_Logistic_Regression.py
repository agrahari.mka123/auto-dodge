from sklearn.linear_model import LogisticRegression
from sklearn.multiclass import OneVsRestClassifier
from sklearn import preprocessing
import numpy as np
import pandas as pd
from datetime import datetime
print(datetime.now())
data = pd.read_csv('uniq.csv')
m,n = data.shape
X = data.values[:,:-1]
def encodeX(X):    
    if(len(X.shape)==1):
       X =  X.reshape(1,len(X))
    enc = preprocessing.OneHotEncoder(sparse=False,dtype=int)
    X_temp = enc.fit_transform(X[:,1:4])
    X = np.concatenate([X[:,0:1],X_temp,X[:,-1:]],axis=1)
    return X
X = encodeX(X)
y = data.values[:,-1].astype(int)
lb = preprocessing.LabelBinarizer()
lb.fit(y)
# classifier = LogisticRegression(multi_class='ovr',max_iter=10000).fit(X,lb.transform(y))
classifier = OneVsRestClassifier(LogisticRegression(max_iter=1000)).fit(X, lb.transform(y))
count=0
for testVal in range(m):
    result = classifier.predict([X[testVal]])[0]
    # print(result)
    if(y[testVal] == np.argmax(result)):
        count=count+1

print(count*100/m)

theta = np.concatenate((classifier.intercept_,classifier.coef_),axis=1)

for theta_i in theta:
        print("[",end=' ')
        print(",".join([str(i) for i in theta_i]),end =' ')
        print("],")

def testFunction():
    # print("main.py")
    print(encodeX(np.array([90,50,1,2,13.01])))
    # print(classifier.predict([encodeX([90,50,1,2,13.01])]))
    # one = classifier.predict([encodeX([104,100,1,2,13.01])]);
    # print(np.argmax(one))
    # print(one)
    # print(classifier.predict([encodeX([64,75,1,2,13.01])]))

testFunction()
print(datetime.now())
