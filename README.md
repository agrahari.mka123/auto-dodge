# Auto-Dodge

 ## Description
In this project, we are trying to make the Chrome dinosaur game play by itself using different machine learning algorithms. In the game, the dinosaur needs to jump through different obstacles (a cactus or a bird) of varying size and height in an accelerating speed. It is challenging because the pattern of obstacles is not uniform, it varies randomly, and same obstacles cannot occur more than 2 times continuously. Likewise, increase in the speed of game, random width and height of obstacles makes it a difficult challenge. 

We have implemented two different methods.
1) Logistic Regression 
2) Neural Network 
_Details will be discussed in the methodology section._


## Related works

Using machine learning to make games automate themselves, has been a common practice in last 4-5 years. 

> The well-known story of TD-gammon is one of the milestones in
> reinforcement learning. It used a model-free TD-learning algorithm
> like Q-learning and achieved human-expert level performance.

Chrome Dinosaur game is famous and easy to experiment in the sense that its code is open sourced and easily available. Learners can tweak the game code to gather the required features. ***We found a [paper](http://cs229.stanford.edu/proj2016/report/KeZhaoWei-AIForChromeOfflineDinosaurGame-report.pdf) by students from Stanford regarding their implementation of different machine learning algorithms within their two semesters of college.*** They concluded that end-to-end Deep-Q learning methods, shows better result than features-extraction based algorithms.

## Methodology

### 1. Logistic regression 
We tried with different number of features at the beginning. We tweaked the game code such that we obtained a bunch of features -  
* the distance with the nearest obstacles
*  size of the obstacle
*  type of the obstacle
* height of the obstacle 
*  speed of the game as the input features.

 We kept the game in an online hosting and made log files of these features set when any person played the game. We collected these features set, when there was no game crash and the dinosaur was moving forward.
We trained our logistic regression model, based on the features collected for a single day game play, and trained a multiclass classifier to predict between a run, jump, duck, duck Begin or a Duck release - and the model, simply did not perform well. It was making more jumps and no ducks. We realized that it could be the case: when training there were a greater number of cactus then the number of birds and hence, it mostly considered only the distance with obstacle as the most contributing factor to predict the final class of output rather than the type of object.

Then, we tweaked the game with a more known pattern of only cactus or bird and trained the data. We got good result when we trained our model for only cactus as the obstacles or only the birds as the obstacles. But when we combined the datasets for these two results, the result was not that good. Again, we suspected it to be because of the discrepancy in the number of training samples we have for the cactus and the birds or for the jumps and ducks. We tried with different possible cases and training samples, but in most of the cases our dinosaur model was predicting a jump for a duck. We tried using an _**if condition**_, to use different sets of thetas for cases: if were a bird or a cactus, and we did get amazing result. But that is not something we could not state to be a complete AI.

 Later, we did some more literature reviews and investigated the already implemented solution of the game on the internet. Different machine learning algorithms like deep learning, reinforcement learning, and neural networks were used, and one common feature in these solutions was that they were trying to dodge the obstacle with only jumps, not the ducks. We used the same approach to our logistic regression model and migrated from a multiclass classifier to a binomial classifier to predict between a run and a jump and we were able to get good results, finally without an _**if condition**_ between a bird and a cactus.
 
![Input and output features of the Logistic Regression model](https://northcentralus1-mediap.svc.ms/transform/thumbnail?provider=spo&inputFormat=png&cs=fFNQTw&docid=https%3A%2F%2Fsaluki-my.sharepoint.com%3A443%2F_api%2Fv2.0%2Fdrives%2Fb!gwlSP7DqDkWUeS_vjqn6VNXCT6jCZaBDhp_elT7_YvmsOYEfp2NvRKWRvMlYygxO%2Fitems%2F01QDK6GQY4Q5YAVOA6UVCJZ3IWEAUNOUET%3Fversion%3DPublished&access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvc2FsdWtpLW15LnNoYXJlcG9pbnQuY29tQGQ1N2E5OGU3LTc0NGQtNDNmOS1iYzkxLTA4ZGUxZmYzNzEwZCIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE1NzYxNzk0NzMiLCJleHAiOiIxNTc2MjAxMDczIiwiZW5kcG9pbnR1cmwiOiJkRHVyZzNOSWxSZWJVYWlTTS93Vnk4OXZ2S0l4WFNXREFpU0R4M0hkNTFnPSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTE2IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJZV1prWXpJd09XWXRPVEF4T0MwNU1EQXdMVGN3TXpNdE56ZGlZakJoTmpsa09USm0iLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiTTJZMU1qQTVPRE10WldGaU1DMDBOVEJsTFRrME56a3RNbVpsWmpobFlUbG1ZVFUwIiwibmFtZWlkIjoiMCMuZnxtZW1iZXJzaGlwfHVybiUzYXNwbyUzYWFub24jNzY4MmE2ODI1NjA5ZGY4NDQwNWIwZDUzYjc0ZWVlZGNiNGY3MTIyOTEwNzBmY2E4M2M3YmMxYTdhMmI3OTE4NyIsIm5paSI6Im1pY3Jvc29mdC5zaGFyZXBvaW50IiwiaXN1c2VyIjoidHJ1ZSIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfHVybiUzYXNwbyUzYWFub24jNzY4MmE2ODI1NjA5ZGY4NDQwNWIwZDUzYjc0ZWVlZGNiNGY3MTIyOTEwNzBmY2E4M2M3YmMxYTdhMmI3OTE4NyIsInNoYXJpbmdpZCI6IjlrQnJYT0pmdGtDRkpxU2hPdWE0aXciLCJ0dCI6IjAiLCJ1c2VQZXJzaXN0ZW50Q29va2llIjoiMiJ9.U2c1REY0WXlCNEpRcVZzbWkrcThPaWZWQjBnd1llMHhHelcxdVVESHU0Yz0&encodeFailures=1&srcWidth=&srcHeight=&width=1920&height=929&action=Access)
### 2. Neural Networks
#### Implementation using TensorFlowJs library

After spending several hours on training, retraining and repeated training of the model using logistic regression, we moved forward to something that allows us to automate the process. We came across a JavaScript library from Google called as the TensorFlow.js (TFJS in short) that allows us to use simple APIs to create and operate on neural networks. 

After implementing neural network in the recent homework 3 assignment, we were thrilled to move on to neural nets and away from Logistic regression. Although we have written our own logic for Logistic regression, we couldn’t do that for neural nets in the interest of time. Maybe sometime later! 

In this approach, like Logistic regression we collect the features from the game while it is playing and use them as input features for our training model. But unlike in Logistic regression where we manually play the game for several instances, collect the data in batch, process them and then get the estimated weights, in neural nets we were able to automate the whole procedure. 

When we looked at the source code of the game, we realized, we can share variables from one game and the other if the page is not refreshed. We decided to make use of this functionality and automate the training process. When we initially begun working on this game, we felt we need to classify the actions of the tRex into 4 classes namely, [Run, Jump, Duck Begin, Duck Release] but as we observed more and more games we came to a conclusion that the concept of ducking can be completely avoided. Therefore, we decided that the output is going to be a 2X1 vector representing whether to run or jump. 

Taking reference from TensorFlow API and some other [blogposts](https://heartbeat.fritz.ai/automating-chrome-dinosaur-game-part-1-290578f13907) we were able to write logic to construct a neural network with a given configuration and use it to fit and predict the actions of the tRex. 

The algorithm chosen to train is rather simple. As discussed before, in case of logistic regression, we collect data after a certain time interval or after every event that happens. But in case of neural net approach, we only add new data after a crash has happened. After every crash, the model tries to figure out what was the state of the game when the crash has happened and which corresponding action of tRex might’ve led to the crash. Now this state is recorded along with what could’ve been done to avoid the crash as the label field of the record.

 **After every crash we do the following:**
 1. Record the features of the game and the corrective measure as the label and we collect these records cumulatively. 
 2.  Try to use the updated features and labels to update the neural net model. 
 3.  Use this updated model to make the decision for the tRex throughout the next game. 
 4. As the number of iterations grow, the tRex tends to become more and more aware of its actions. 
_However, these changes cannot be evidently visible while playing, since the way the obstacles come is still random._
### Finding the right configuration?

This is one of the areas where we found using neural nets as completely different from that of Logistic regression. Here, the training process happens in an iterative approach, where the model tends to learn more and more as the games increase. Whereas in Logistic regression, the approach was more batch oriented. Since we were now able to automate the process of training, we decided to run the project on multiple machines and each machine having a different configuration. We added some GUI elements to the application so that the user can decide how he wants to train the Neural net. We’ve provided an interface where the user can choose all the features based on which he wishes the model to be trained on. The output is fixed for now, a 2 x 1 vector which mentions whether the tRex should jump or keep running given a certain feature vector. Now one more thing that is configurable is the Hidden layers and their sizes.

 On trying various combinations of features and various neural network architectures, we observed that the performance of the tRex was better when there is a single hidden layer between input and the output and the size of the layer is slightly bigger than that of the input layer. (_Our vision is currently short sighted! :P_)


![A Sample Neural network](http://www.swarthmore.edu/NatSci/mzucker1/opencv-2.4.10-docs/_images/mlp.png)

## 2. Software Requirements - Do I need to install software/packages?

-	All you need is a latest Chrome/Firefox browser.

## 3. Tool/Project Installation Instructions

- The main zip file consists of two sub-folders auto-dodge-LR and auto-dodge-NN which represents Logistic Regression and Neural Network approaches. 
-  Each folder consists of an index.html file. Select this and use open with a browser of your choice. 
-  There are some approach specific instructions in the following
	- **Logistic Regression**
		- To view and run the application – a good browser 
		- To collect data and train the model – Apache Server with PHP interaction and also python3. 
	- **Neural Network**
		-  Just a browser and internet connection (cached tensorflow.js from online source) would suffice

## 4. Instructions for Generating Results


- Logistic Regression 
	- If you wish to see the performance of the best model we found, you need to open index.js file and search for “aiPlay”(line 87) and make sure that the initial value of aiPlay is given as true. Then running the index.html file would be sufficient. 
	-  If you want to train a model and test it on your own (which we don’t expect you to do) you would need an Apache server capable of serving PHP pages and also python3 installed in the machine. 
	-  The auto-dodge-LR folder consists of a [classifier.py](./LR/classifier.py) folder and a [data.csv](./LR/data.csv) 
	-  One needs to open the [index.js](./LR/index.js) file and set the variable ***shouldLog*** to true. 
	-  At the end of the game, if they see, a prompt asking if they want to send data to backend, then they must choose yes. 
	-  After carefully training the model for a bunch of games, the algorithm would be capable to perform the classification. 
	-  Run the python file  [classifier.py](./LR/classifier.py) which will print theta on the screen. 
	-  Copy this theta and paste this in the giveDecision method present in index.js 
	-  The performance of the output largely depends on the quality of the training data and we cannot guarantee that any randomly generated theta would be suitable for the game.


- Neural Network 
	- Here the user must first decide the configuration using which he wants to train the model. 
	-  When you open index.html, you would notice a Checkbox group and a textbox waiting for your input 
	-  From the given features check all those features that you would like to have in your model. 
	-  Now, coming to the second input field. Here we expect you to give the architecture of your neural network. 
	-  Since the input features are already selected and the output size is always 2, we will only have to arrange the hidden layers appropriately. 
	- The hidden layers are expected to be given as a csv input into the field. 
		- Example: - 4, 5, 3 in the textbox represents that, the neural network is going to have three hidden layers each of size 4, 5 and 3 respectively.
	-  When completed the inputs, click on the submit button and follow the instructions mentioned in the alert box carefully. 
	-  We could not add any client side validation and therefore we expect you follow all the given instructions carefully. 
	-  As the number of games progresses the learning rate of model also increases. 
	-  Nevertheless, the performance of the tRex in this game is also dependent on appropriate feature selection and suitable neural network architecture.

## 5. Folder Contents (main files, figures, videos etc.)

- All the video recordings while building the project are too big to be fit in this submission, hence we’ve added them up in OneDrive with an Open link - [Click here to access](https://saluki-my.sharepoint.com/personal/manojkumar_agrahari_siu_edu/_layouts/15/onedrive.aspx?id=/personal/manojkumar_agrahari_siu_edu/Documents/Machine%20Learning/AutoDodge%20Project%20Submission&originalPath=aHR0cHM6Ly9zYWx1a2ktbXkuc2hhcmVwb2ludC5jb20vOmY6L2cvcGVyc29uYWwvbWFub2prdW1hcl9hZ3JhaGFyaV9zaXVfZWR1L0VpVW9XSFp3OTNsTHRMQVEtMlNnd0lzQmh6cmkxRXZqRGpSWnl0N0tNanBIWnc_cnRpbWU9X3praUp6NV8xMGc) (_Still working on providing public access to this link_)
